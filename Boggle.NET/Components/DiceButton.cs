﻿using System.Windows.Forms;

namespace Boggle.NET.Components
{
    public class DiceButton : Button
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Letter { get; set; }
    }
}
