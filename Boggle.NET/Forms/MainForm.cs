﻿using Boggle.NET.Components;
using Boggle.NET.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Boggle.NET.Forms
{
    public partial class MainForm : Form
    {
        private readonly int size = 4;
        private DiceButton[,] GameDices { get; set; }
        private List<DiceButton> SelectedDices { get; set; } = new List<DiceButton>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void StartNewGame_Click(object sender, EventArgs e)
        {
            CurrentWordTextBox.Text = "";
            FoundWordsList.Clear();
            ScoreTextBox.Text = "0";
            GameBoardTable.Controls.Clear();
            InitializeGameDices(size);
        }

        private void InitializeGameDices(int size)
        {
            GameDices = new DiceButton[size, size];
            string[] letters = Helper.GenerateRandomLetters(size * size);
            int letterIndex = 0;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    GameDices[x, y] = Helper.CreateDiceButton(x, y, letters[letterIndex]);
                    GameDices[x, y].Click += new EventHandler(DiceButton_Click);
                    GameBoardTable.Controls.Add(GameDices[x, y], x, y);
                    letterIndex++;
                }
            }
        }

        private string GetCurrentWord()
        {
            return SelectedDices.Count > 0 ? SelectedDices.Aggregate("", (acc, current) => acc += current.Letter) : "";
        }

        private void DiceButton_Click(object sender, EventArgs e)
        {
            DiceButton dice = sender as DiceButton;
            bool inSelected = SelectedDices.Any(d => d == dice);
            // Deselect
            if (inSelected)
            {
                if (ValidateDiceDeselect(dice))
                {
                    SelectedDices.Remove(dice);
                    SetDiceStyle(dice, false);
                }
                else
                {
                    MessageBox.Show("You can only de-select last chosen dice.", "Invalid de-select", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            // Select
            else
            {
                if (ValidateDiceSelect(dice))
                {
                    SelectedDices.Add(dice);
                    SetDiceStyle(dice, true);
                }
                else
                {
                    MessageBox.Show("Please select adjacent dice neighbouring the current one.", "Invalid select", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            CurrentWordTextBox.Text = GetCurrentWord();
        }

        private bool ValidateDiceSelect(DiceButton dice)
        {
            if (SelectedDices.Count == 0)
            {
                return true;
            }
            DiceButton lastSelectedDice = SelectedDices[SelectedDices.Count - 1];
            List<int> validX = new List<int>() { lastSelectedDice.X - 1, lastSelectedDice.X, lastSelectedDice.X + 1 };
            List<int> validY = new List<int>() { lastSelectedDice.Y - 1, lastSelectedDice.Y, lastSelectedDice.Y + 1 };
            return validX.Any(x => x == dice.X) && validY.Any(y => y == dice.Y);
        }

        private bool ValidateDiceDeselect(DiceButton dice)
        {
            DiceButton lastSelectedDice = SelectedDices[SelectedDices.Count - 1];
            return dice == lastSelectedDice;
        }

        private void SetDiceStyle(DiceButton dice, bool selected)
        {
            if (selected)
            {
                dice.BackColor = Color.Blue;
                dice.ForeColor = Color.White;
            }
            else
            {
                dice.BackColor = Color.WhiteSmoke;
                dice.ForeColor = Color.Black;
                dice.UseVisualStyleBackColor = true;
            }
        }

        private void AddToList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(CurrentWordTextBox.Text))
            {
                FoundWordsList.Items.Add(CurrentWordTextBox.Text);
                List<string> words = new List<string>();
                foreach (ListViewItem item in FoundWordsList.Items)
                {
                    words.Add(item.Text);
                }
                ScoreTextBox.Text = ScoreUtility.CalculateScore(words).ToString();
                CurrentWordTextBox.Text = "";
                ResetDiceSelection();
            }
        }

        private void ResetDiceSelection()
        {
            SelectedDices.ForEach(d => SetDiceStyle(d, false));
            SelectedDices.Clear();
        }
    }
}
