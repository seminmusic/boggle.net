﻿namespace Boggle.NET.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GameBoardTable = new System.Windows.Forms.TableLayoutPanel();
            this.StartNewGame = new System.Windows.Forms.Button();
            this.FoundWordsList = new System.Windows.Forms.ListView();
            this.CurrentWordLabel = new System.Windows.Forms.Label();
            this.FoundWordsLabel = new System.Windows.Forms.Label();
            this.CurrentWordTextBox = new System.Windows.Forms.TextBox();
            this.AddToList = new System.Windows.Forms.Button();
            this.ScoreLabel = new System.Windows.Forms.Label();
            this.ScoreTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // GameBoardTable
            // 
            this.GameBoardTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.GameBoardTable.ColumnCount = 4;
            this.GameBoardTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.Location = new System.Drawing.Point(12, 12);
            this.GameBoardTable.Name = "GameBoardTable";
            this.GameBoardTable.RowCount = 4;
            this.GameBoardTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.GameBoardTable.Size = new System.Drawing.Size(300, 300);
            this.GameBoardTable.TabIndex = 1;
            // 
            // StartNewGame
            // 
            this.StartNewGame.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StartNewGame.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartNewGame.Location = new System.Drawing.Point(325, 12);
            this.StartNewGame.Name = "StartNewGame";
            this.StartNewGame.Size = new System.Drawing.Size(260, 29);
            this.StartNewGame.TabIndex = 3;
            this.StartNewGame.TabStop = false;
            this.StartNewGame.Text = "START NEW GAME";
            this.StartNewGame.UseVisualStyleBackColor = true;
            this.StartNewGame.Click += new System.EventHandler(this.StartNewGame_Click);
            // 
            // FoundWordsList
            // 
            this.FoundWordsList.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FoundWordsList.Location = new System.Drawing.Point(325, 139);
            this.FoundWordsList.Name = "FoundWordsList";
            this.FoundWordsList.Size = new System.Drawing.Size(260, 127);
            this.FoundWordsList.TabIndex = 4;
            this.FoundWordsList.UseCompatibleStateImageBehavior = false;
            this.FoundWordsList.View = System.Windows.Forms.View.List;
            // 
            // CurrentWordLabel
            // 
            this.CurrentWordLabel.AutoSize = true;
            this.CurrentWordLabel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentWordLabel.Location = new System.Drawing.Point(322, 53);
            this.CurrentWordLabel.Name = "CurrentWordLabel";
            this.CurrentWordLabel.Size = new System.Drawing.Size(97, 19);
            this.CurrentWordLabel.TabIndex = 5;
            this.CurrentWordLabel.Text = "Current word:";
            // 
            // FoundWordsLabel
            // 
            this.FoundWordsLabel.AutoSize = true;
            this.FoundWordsLabel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FoundWordsLabel.Location = new System.Drawing.Point(322, 117);
            this.FoundWordsLabel.Name = "FoundWordsLabel";
            this.FoundWordsLabel.Size = new System.Drawing.Size(95, 19);
            this.FoundWordsLabel.TabIndex = 6;
            this.FoundWordsLabel.Text = "Found words:";
            // 
            // CurrentWordTextBox
            // 
            this.CurrentWordTextBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentWordTextBox.Location = new System.Drawing.Point(325, 75);
            this.CurrentWordTextBox.Name = "CurrentWordTextBox";
            this.CurrentWordTextBox.ReadOnly = true;
            this.CurrentWordTextBox.Size = new System.Drawing.Size(176, 27);
            this.CurrentWordTextBox.TabIndex = 7;
            // 
            // AddToList
            // 
            this.AddToList.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddToList.Location = new System.Drawing.Point(507, 74);
            this.AddToList.Name = "AddToList";
            this.AddToList.Size = new System.Drawing.Size(78, 29);
            this.AddToList.TabIndex = 8;
            this.AddToList.Text = "ADD";
            this.AddToList.UseVisualStyleBackColor = true;
            this.AddToList.Click += new System.EventHandler(this.AddToList_Click);
            // 
            // ScoreLabel
            // 
            this.ScoreLabel.AutoSize = true;
            this.ScoreLabel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScoreLabel.Location = new System.Drawing.Point(422, 288);
            this.ScoreLabel.Name = "ScoreLabel";
            this.ScoreLabel.Size = new System.Drawing.Size(57, 19);
            this.ScoreLabel.TabIndex = 9;
            this.ScoreLabel.Text = "SCORE:";
            // 
            // ScoreTextBox
            // 
            this.ScoreTextBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScoreTextBox.Location = new System.Drawing.Point(485, 285);
            this.ScoreTextBox.Name = "ScoreTextBox";
            this.ScoreTextBox.ReadOnly = true;
            this.ScoreTextBox.Size = new System.Drawing.Size(100, 27);
            this.ScoreTextBox.TabIndex = 10;
            this.ScoreTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 323);
            this.Controls.Add(this.ScoreTextBox);
            this.Controls.Add(this.ScoreLabel);
            this.Controls.Add(this.AddToList);
            this.Controls.Add(this.CurrentWordTextBox);
            this.Controls.Add(this.FoundWordsLabel);
            this.Controls.Add(this.CurrentWordLabel);
            this.Controls.Add(this.FoundWordsList);
            this.Controls.Add(this.StartNewGame);
            this.Controls.Add(this.GameBoardTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Boggle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GameBoardTable;
        private System.Windows.Forms.Button StartNewGame;
        private System.Windows.Forms.ListView FoundWordsList;
        private System.Windows.Forms.Label CurrentWordLabel;
        private System.Windows.Forms.Label FoundWordsLabel;
        private System.Windows.Forms.TextBox CurrentWordTextBox;
        private System.Windows.Forms.Button AddToList;
        private System.Windows.Forms.Label ScoreLabel;
        private System.Windows.Forms.TextBox ScoreTextBox;
    }
}

