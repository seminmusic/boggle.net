﻿using Boggle.NET.Components;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Boggle.NET.Utilities
{
    public class Helper
    {
        public static string[] GenerateRandomLetters(int numberOfLetters)
        {
            string[] letters = new string[numberOfLetters];
            Random random = new Random();
            for (int i = 0; i < numberOfLetters; i++)
            {
                int num = random.Next(0, 26);
                char letter = (char)('a' + num);
                letters[i] = letter.ToString();
            }
            return letters;
        }

        public static DiceButton CreateDiceButton(int x, int y, string letter)
        {
            return new DiceButton()
            {
                X = x,
                Y = y,
                Letter = letter.ToUpper(),
                Size = new Size(67, 67),
                Text = letter.ToUpper(),
                Font = new Font("Calibri", 30F, FontStyle.Regular, GraphicsUnit.Point, 0),
                BackColor = Color.WhiteSmoke,
                ForeColor = Color.Black,
                UseVisualStyleBackColor = true,
                Cursor = Cursors.Hand,
                TabStop = false
            };
        }
    }
}
