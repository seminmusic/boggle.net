﻿using Boggle.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Boggle.NET.Utilities
{
    public static class ScoreUtility
    {

        /// <summary>
        /// Calculates total score for each player based on multiplayer Boggle rules (assume that all players words given are valid).
        /// </summary>
        /// <param name="players">List of Player models</param>
        public static void CalculatePlayersScores(List<Player> players)
        {
            if (players == null)
            {
                throw new ArgumentNullException("players");
            }
            if (players.Count == 0)
            {
                return;
            }

            List<string> allWords = players.Select(p => p.Words)
                                           .Aggregate(new List<string>(), (acc, list) => acc.Concat(list).ToList());

            List<string> nonDuplicatedWords = allWords.Where(w => !string.IsNullOrWhiteSpace(w) && allWords.Count(a => !string.IsNullOrWhiteSpace(a) && a.Trim().ToLower() == w.Trim().ToLower()) == 1)
                                                      .Select(w => w.Trim().ToLower())
                                                      .Distinct()
                                                      .ToList();
            foreach (Player player in players)
            {
                List<string> playerUniqueWords = player.Words
                                                       .Where(w => !string.IsNullOrWhiteSpace(w) && nonDuplicatedWords.Any(n => w.Trim().ToLower() == n))
                                                       .ToList();

                player.Score = CalculateScore(playerUniqueWords);
            }
        }

        /// <summary>
        /// Calculates total score based on Boggle rules (assume that all words given are valid).
        /// </summary>
        /// <param name="words">List of words</param>
        /// <returns>Total score</returns>
        public static int CalculateScore(List<string> words)
        {
            if (words == null)
            {
                throw new ArgumentNullException("words");
            }
            if (words.Count == 0)
            {
                return 0;
            }
            List<string> uniqueWords = words.Where(w => !string.IsNullOrWhiteSpace(w))
                                            .Select(w => w.Trim().ToLower())
                                            .Distinct()
                                            .ToList();
            int totalScore = 0;
            uniqueWords.ForEach(word => totalScore += GetWordPoints(word));
            return totalScore;
        }

        /// <summary>
        /// Get single word points based on Boggle rules (assume that word is valid).
        /// </summary>
        /// <param name="word">Single word</param>
        /// <returns>Word points</returns>
        public static int GetWordPoints(string word)
        {
            if (word == null)
            {
                throw new ArgumentNullException("word");
            }
            string w = word.Trim();
            if (w.Length < 3)
            {
                return 0;
            }
            if (w.Length >= 8)
            {
                return 11;
            }
            switch (w.Length)
            {
                case 3:
                case 4:
                    return 1;
                case 5:
                    return 2;
                case 6:
                    return 3;
                case 7:
                    return 5;
                default:
                    return 0;
            }
        }

    }
}
