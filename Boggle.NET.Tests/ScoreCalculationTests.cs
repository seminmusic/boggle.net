﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boggle.NET.Utilities;
using System;
using System.Collections.Generic;
using Boggle.NET.Models;

namespace Boggle.NET.Tests
{
    [TestClass]
    public class ScoreCalculationTests
    {

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetWordPoints_Argument_Null()
        {
            string word = null;
            ScoreUtility.GetWordPoints(word);
        }

        [TestMethod]
        public void GetWordPoints_Argument_Not_Null()
        {
            string word1 = " ab ";
            int points1 = ScoreUtility.GetWordPoints(word1);
            Assert.AreEqual(points1, 0);

            string word2 = " phsek ";
            int points2 = ScoreUtility.GetWordPoints(word2);
            Assert.AreEqual(points2, 2);

            string word3 = " abcdedsq ";
            int points3 = ScoreUtility.GetWordPoints(word3);
            Assert.AreEqual(points3, 11);

            string word4 = " etuwedgtpohdq ";
            int points4 = ScoreUtility.GetWordPoints(word4);
            Assert.AreEqual(points4, 11);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateScore_Argument_Null()
        {
            List<string> words = null;
            ScoreUtility.CalculateScore(words);
        }

        [TestMethod]
        public void CalculateScore_Argument_Not_Null()
        {
            List<string> words1 = new List<string>();
            int score1 = ScoreUtility.CalculateScore(words1);
            Assert.AreEqual(score1, 0);

            List<string> words2 = new List<string>()
            {
                "", "   "
            };
            int score2 = ScoreUtility.CalculateScore(words2);
            Assert.AreEqual(score2, 0);

            List<string> words3 = new List<string>()
            {
                "semin", " SeMiN "
            };
            int score3 = ScoreUtility.CalculateScore(words3);
            Assert.AreEqual(score3, 2);

            List<string> words4 = new List<string>()
            {
                null, "", "se", "sem", "semi", "  semin  ", " ", "seminmu", "seminmus", "seminmusic", "seMin"
            };
            int score4 = ScoreUtility.CalculateScore(words4);
            Assert.AreEqual(score4, 31);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculatePlayersScores_Argument_Null()
        {
            List<Player> players = null;
            ScoreUtility.CalculatePlayersScores(players);
        }

        [TestMethod]
        public void CalculatePlayersScores_Argument_Not_Null()
        {
            List<Player> players1 = new List<Player>();
            ScoreUtility.CalculatePlayersScores(players1);
            foreach (Player player in players1)
            {
                Assert.AreEqual(player.Score, 0);
            }

            List<Player> players2 = new List<Player>()
            {
                new Player()
                {
                    Id = 1, Name = "First",
                    Words = new List<string>() { "aaa", null, " ", "bbb", "ccc" }
                },
                new Player()
                {
                    Id = 2, Name = "Second",
                    Words = new List<string>() { "cccccc", "ddd" }
                },
                new Player()
                {
                    Id = 3, Name = "Third",
                    Words = new List<string>() { "eee", "Ccc", "fff" }
                },
                new Player()
                {
                    Id = 4, Name = "Lucas",
                    Words = new List<string>() { "am", "bibble", "loo", "", " ", "malarkey", "nudiustertian", "quire", "widdershins", "xertz", "bloviate", "pluto" }
                },
                new Player()
                {
                    Id = 5, Name = "Clara",
                    Words = new List<string>() { "xertz", "gardyloo", "catty", "fuzzle", "mars", "sialoquent", "quire", "lollygag", "colly", "taradiddle", "snickersnee", "widdershins", "gardy" }
                },
                new Player()
                {
                    Id = 6, Name = "Klaus",
                    Words = new List<string>() { "bumfuzzle", "wabbit", "catty", null, "flibbertigibbet", "am", "loo", "wampus", "bibble", "nudiustertian", "xertz" }
                },
                new Player()
                {
                    Id = 7, Name = "Raphael",
                    Words = new List<string>() { "bloviate", "loo", "xertz", "mars", "erinaceous", "wampus", "am", "bibble", "cattywampus" }
                },
                new Player()
                {
                    Id = 8, Name = "Tom",
                    Words = new List<string>() { "bibble", "loo", "snickersnee", "quire", "am", "malarkey" }
                }
            };
            ScoreUtility.CalculatePlayersScores(players2);
            List<int> expectedScores = new List<int>() { 2, 4, 2, 2, 51, 25, 22, 0 };

            Assert.AreEqual(expectedScores[0], players2[0].Score);
            Assert.AreEqual(expectedScores[1], players2[1].Score);
            Assert.AreEqual(expectedScores[2], players2[2].Score);
            Assert.AreEqual(expectedScores[3], players2[3].Score);
            Assert.AreEqual(expectedScores[4], players2[4].Score);
            Assert.AreEqual(expectedScores[5], players2[5].Score);
            Assert.AreEqual(expectedScores[6], players2[6].Score);
            Assert.AreEqual(expectedScores[7], players2[7].Score);
        }

    }
}
